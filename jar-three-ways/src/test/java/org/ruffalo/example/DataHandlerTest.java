package org.ruffalo.example;

import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.junit5.WireMockRuntimeInfo;
import com.github.tomakehurst.wiremock.junit5.WireMockTest;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.core.MediaType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.ruffalo.example.dto.ServiceDescriptionRow;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

@WireMockTest
public class DataHandlerTest {

    /**
     * Return a 404 when nothing is found. Shows that errors bubble out of the client.
     *
     * @param wireMockRuntimeInfo injected by junit to provide information on the wiremock instance
     */
    @Test
    public void error404(WireMockRuntimeInfo wireMockRuntimeInfo) {
        final DataHandler handler = new DataHandler();
        Assertions.assertThrows(NotFoundException.class, () -> { handler.getServiceRows(wireMockRuntimeInfo.getHttpBaseUrl());  });
    }

    /**
     * Test the short meters file
     *
     * @param wireMockRuntimeInfo injected by junit to provide information on the wiremock instance
     * @throws IOException when the test file cannot be read
     */
    @Test
    public void metersShort(WireMockRuntimeInfo wireMockRuntimeInfo) throws IOException {
        final DataHandler handler = new DataHandler();
        final List<ServiceDescriptionRow> rows = rows("Meters-Short.json", wireMockRuntimeInfo);
        Assertions.assertEquals(100, rows.size());

        // convert
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        handler.write(rows, baos);
        final byte[] bytes = baos.toByteArray();
        Assertions.assertTrue(0 < bytes.length); // would like for this to be better
    }


    /**
     * Test the short services file
     *
     * @param wireMockRuntimeInfo injected by junit to provide information on the wiremock instance
     * @throws IOException when the test file cannot be read
     */
    @Test
    public void servicesShort(WireMockRuntimeInfo wireMockRuntimeInfo) throws IOException {
        final DataHandler handler = new DataHandler();
        final List<ServiceDescriptionRow> rows = rows("Services-Short.json", wireMockRuntimeInfo);
        Assertions.assertEquals(100, rows.size());

        // convert
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        handler.write(rows, baos);
        final byte[] bytes = baos.toByteArray();
        Assertions.assertTrue(0 < bytes.length); // would like for this to be better
    }

    /**
     * Test the location descriptions file
     *
     * @param wireMockRuntimeInfo injected by junit to provide information on the wiremock instance
     * @throws IOException when the test file cannot be read
     */
    @Test
    public void locationDescriptions(WireMockRuntimeInfo wireMockRuntimeInfo) throws IOException {
        final DataHandler handler = new DataHandler();
        final List<ServiceDescriptionRow> rows = rows("LocationDescriptions.json", wireMockRuntimeInfo);
        Assertions.assertEquals(30823, rows.size());

        // convert
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        handler.write(rows, baos);
        final byte[] bytes = baos.toByteArray();
        Assertions.assertTrue(0 < bytes.length); // would like for this to be better
    }

    /**
     * Sets up the wire mock to serve the given file from the root of the wiremock web server and
     * then requests that file from the root.
     *
     * @param file in src/test/resources to serve and get with the client
     * @param wireMockRuntimeInfo used to determine where the root of the server is
     * @return a list of service description rows
     * @throws IOException when the file cannot be rad
     */
    private List<ServiceDescriptionRow> rows(final String file, WireMockRuntimeInfo wireMockRuntimeInfo) throws IOException {
        WireMock.stubFor(WireMock.get("/")
            .willReturn(
                WireMock.ok(Files.readString(Paths.get(String.format("src/test/resources/%s", file))
            )).withHeader("Content-type", MediaType.APPLICATION_JSON)));
        final DataHandler handler = new DataHandler();
        return handler.getServiceRows(wireMockRuntimeInfo.getHttpBaseUrl());
    }
}
