package org.ruffalo.example;

import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.core.GenericType;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.ruffalo.example.dto.ServiceDescriptionRow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.List;

public class DataHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(DataHandler.class);

    private static final List<String> CSV_HEADERS = new LinkedList<>() {
        {
            add("map_number");
            add("service_type");
            add("rate_code");
            add("account_number");
            add("disconnect_date");
            add("account_status");
            add("phone_number");
            add("reading_date");
            add("customer_name");
            add("street_number");
            add("street_name");
            add("street_type");
            add("street_unit");
            add("address_city");
            add("address_state");
            add("address_zip");
            add("meter_number");
        }
    };

    public List<ServiceDescriptionRow> getServiceRows(final String url) {
        try (Client client = ResteasyClientBuilder.newClient()) {
            return client.target(url).request().get(new GenericType<>() {
            });
        }
    }

    public void write(List<ServiceDescriptionRow> rows, OutputStream to) throws IOException {
        // write headers
        to.write(String.join(",", CSV_HEADERS).getBytes(StandardCharsets.UTF_8));
        // write all rows
        for (final ServiceDescriptionRow serviceDescriptionRow : rows) {
            to.write("\n".getBytes(StandardCharsets.UTF_8));
            to.write(serviceDescriptionRow.toCsvRow().getBytes(StandardCharsets.UTF_8));
        }
    }
}
