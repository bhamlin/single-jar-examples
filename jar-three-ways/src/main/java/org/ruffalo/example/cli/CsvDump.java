package org.ruffalo.example.cli;

import picocli.CommandLine;

@CommandLine.Command(name = "csv-dumper", description = "A command to grab a JSON data structure from a given URL and dump it to CSV", mixinStandardHelpOptions = true, version = "1.0.0", subcommands = {
        DownloadCommand.ServiceTypes.class,
        DownloadCommand.LocationDescriptions.class,
        DownloadCommand.MeterMultipliers.class
}, subcommandsRepeatable = true)
public class CsvDump {

}
