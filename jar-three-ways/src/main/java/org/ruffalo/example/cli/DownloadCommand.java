package org.ruffalo.example.cli;

import org.ruffalo.example.DataHandler;
import org.ruffalo.example.dto.ServiceDescriptionRow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import picocli.CommandLine;

import java.io.File;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.util.List;
import java.util.concurrent.Callable;

public abstract class DownloadCommand implements Callable<Integer> {

    private static final DataHandler HANDLER = new DataHandler();

    @CommandLine.Command(name = "service-types", description = "Download service types", mixinStandardHelpOptions = true)
    public static class ServiceTypes extends DownloadCommand {

    }

    @CommandLine.Command(name = "loc-desc", description = "Download location descriptions", mixinStandardHelpOptions = true)
    public static class LocationDescriptions extends DownloadCommand {

    }

    @CommandLine.Command(name = "multipliers", description = "Download meter multipliers", mixinStandardHelpOptions = true)
    public static class MeterMultipliers extends DownloadCommand {

    }

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @CommandLine.Option(names = { "-o",
            "--output" }, description = "The output file that the CSV data will be written to. If no output file is specified the data will be written to STDOUT.")
    File outputFile;

    @CommandLine.Option(names = { "-u", "--url",
            "--uri" }, description = "The target URL to download JSON data from", required = true)
    String URI;

    @CommandLine.Option(names = { "-f",
            "--force" }, description = "Set to force overwrite of the target output file.", fallbackValue = "false")
    boolean force;

    @Override
    public Integer call() throws Exception {
        final String uriString = this.getURI();
        final OutputStream output;

        // try and parse url
        try {
            new URI(uriString);
        } catch (URISyntaxException use) {
            LOGGER.error("failed to parse input URL as a valid URL '{}'", uriString, use);
            return 1;
        }

        if (outputFile != null) {
            if (outputFile.isFile()) {
                if (outputFile.exists() && !isForce()) {
                    LOGGER.error("The file '{}' already exists, use the --force command to overwrite",
                            outputFile.getPath());
                    return 1;
                } else if (outputFile.exists()) {
                    LOGGER.warn("The file '{}' already exists, overwriting", outputFile.getPath());
                }
            } else if (outputFile.isDirectory()) {
                LOGGER.error("The output path '{}' is a directory, cannot write output", outputFile.getPath());
                return 1;
            }
            output = Files.newOutputStream(outputFile.toPath());
        } else {
            output = System.out;
        }

        // ensure that it is closed using the autoclose mechanism
        try (output) {
            final List<ServiceDescriptionRow> rows = HANDLER.getServiceRows(uriString);
            HANDLER.write(rows, output);
        } catch (Exception ex) {
            LOGGER.error("failed to collect and write JSON data", ex);
            return 1;
        }
        return 0;
    }

    public File getOutputFile() {
        return outputFile;
    }

    public void setOutputFile(File outputFile) {
        this.outputFile = outputFile;
    }

    public String getURI() {
        return URI;
    }

    public void setURI(String URI) {
        this.URI = URI;
    }

    public boolean isForce() {
        return force;
    }

    public void setForce(boolean force) {
        this.force = force;
    }
}
