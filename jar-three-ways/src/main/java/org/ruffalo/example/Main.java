package org.ruffalo.example;

import org.ruffalo.example.cli.CsvDump;
import picocli.CommandLine;

public class Main {

    public static void main(String[] args) {
        final CommandLine commandLine = new CommandLine(new CsvDump());
        final CommandLine.ParseResult result = commandLine.parseArgs(args);
        if (result.isUsageHelpRequested()) {
            commandLine.usage(System.out);
            System.exit(0);
        } else if (result.isVersionHelpRequested()) {
            commandLine.printVersionHelp(System.out);
            System.exit(0);
        }
        commandLine.execute(args);
    }

}
