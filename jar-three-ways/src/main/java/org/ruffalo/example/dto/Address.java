package org.ruffalo.example.dto;

public class Address extends CsvElement {

    private String streetNumber;

    private String streetName;

    private String streetType;

    private String unit;

    private String city;

    private String state;

    private String zip;

    public String getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getStreetType() {
        return streetType;
    }

    public void setStreetType(String streetType) {
        this.streetType = streetType;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String toCsvRow() {
        final StringBuilder builder = new StringBuilder();
        quoted(builder, orBlank(streetNumber)).append(",");
        quoted(builder, orBlank(streetName)).append(",");
        quoted(builder, orBlank(streetType)).append(",");
        quoted(builder, orBlank(unit)).append(",");
        quoted(builder, orBlank(city)).append(",");
        quoted(builder, orBlank(state)).append(",");
        quoted(builder, orBlank(zip));
        return builder.toString();
    }
}
