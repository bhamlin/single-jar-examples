package org.ruffalo.example.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

// from https://gitlab.com/bhamlin/csv-dumper/-/blob/main/src/model.rs
@JsonIgnoreProperties(ignoreUnknown = true)
public class ServiceDescriptionRow extends CsvElement {

    private String mapNumber;

    private String serviceType;

    private String rateCode;

    private String accountNumber;

    private String disconnectDate;

    private String accountStatus;

    private String phone;

    private String readingDate;

    private String memberName;

    private Address address;

    private String meterNumber;

    public String getMapNumber() {
        return mapNumber;
    }

    public void setMapNumber(String mapNumber) {
        this.mapNumber = mapNumber;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getRateCode() {
        return rateCode;
    }

    public void setRateCode(String rateCode) {
        this.rateCode = rateCode;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getDisconnectDate() {
        return disconnectDate;
    }

    public void setDisconnectDate(String disconnectDate) {
        this.disconnectDate = disconnectDate;
    }

    public String getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(String accountStatus) {
        this.accountStatus = accountStatus;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getReadingDate() {
        return readingDate;
    }

    public void setReadingDate(String readingDate) {
        this.readingDate = readingDate;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getMeterNumber() {
        return meterNumber;
    }

    public void setMeterNumber(String meterNumber) {
        this.meterNumber = meterNumber;
    }

    public String toCsvRow() {
        final StringBuilder builder = new StringBuilder();
        quoted(builder, orBlank(mapNumber)).append(",");
        quoted(builder, orBlank(serviceType)).append(",");
        quoted(builder, orBlank(rateCode)).append(",");
        quoted(builder, orBlank(accountNumber)).append(",");
        quoted(builder, orBlankSubstring(disconnectDate, 10)).append(",");
        quoted(builder, orBlank(phone)).append(",");
        quoted(builder, orBlankSubstring(readingDate, 10)).append(",");
        quoted(builder, orBlank(memberName)).append(",");
        if (address == null) {
            address = new Address();
        }
        builder.append(address.toCsvRow());
        quoted(builder, orBlank(meterNumber));

        return builder.toString();
    }


}
