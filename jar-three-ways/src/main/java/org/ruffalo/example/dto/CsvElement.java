package org.ruffalo.example.dto;

import java.util.Optional;

public abstract class CsvElement {

    protected StringBuilder quoted(final StringBuilder buffer, final String input) {
        return buffer.append("\"").append(input).append("\"");
    }

    protected String orBlankSubstring(final String input, int chars) {
        return Optional.ofNullable(input).filter(c -> !c.trim().isEmpty()).map(c -> {
            if (c.length() > chars) {
                return input.substring(0, chars);
            }
            return input;
        }).orElse("");
    }

    protected String orBlank(final String input) {
        return Optional.ofNullable(input).filter(c -> !c.trim().isEmpty()).orElse("");
    }

}
