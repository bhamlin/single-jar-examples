# (Executable) JAR Three Ways

A simple maven project that creates three different jars using different maven plugins. Each jar has its
own way of loading dependencies.

## Building
A simple `mvn clean package` is enough.

## External Dependencies
Uses the maven dependency plugin and maven jar plugin to create a jar with external dependencies copied to 
the "target/lib" directory. The jar, when executed, has manifest entries that cause it to look for jars
in an adjacent "lib" directory. As long as you copy the jar around with the sibling "lib" directory it will
be executed with all the dependencies it needs. This is the most reliable way to distribute because it works
with Java modules and all the SPI stuff out of the box.

```bash
[]$ java -jar target/jar-three-ways-1.0-SNAPSHOT-external.jar --help
```

## JAR with Dependencies
This is a method that just uses the maven assembly plugin to jam all the dependencies inside the
jar itself. This is not quite as flexibly as other methods but it works in simple cases. The maven
assembly plugin is very good at building jar (zip, war, ear) structures and serves as the backbone
of the way that maven builds artifacts in a lot of cases but it is not usually very flexible.

```bash
[]$ java -jar target/jar-three-ways-1.0-SNAPSHOT-jar-with-dependencies.jar --help
```

## Shaded JAR
A shaded jar is one that contains all the classes and resources within itself from the original jar and dependencies but
managed in a way that tries to reconcile files with the same name/path. The shaded plugin can transform files by appending
them or it can exclude dependencies or files altogether. It is a more powerful version of the "jar-with-dependencies" method.

The main issues here are that it breaks encapsulation of Java modules and should never be used by another project as a dependency.
It may also have issues with property files (application.properties, message.properties) that need to be appended together or with
xml configuration/value files. The transformers can help with that but may not always work the way you need. Overall this is
my preferred method but if it causes issues I would skip straight to external dependencies.

```bash
[]$ java -jar target/jar-three-ways-1.0-SNAPSHOT-shaded.jar --help
```